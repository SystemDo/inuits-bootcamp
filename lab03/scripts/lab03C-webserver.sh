#!/bin/sh
mkdir /app
cp -r /vagrant/files/python-app /app/
cd /app/python-app
pip3 install pipenv
/usr/local/bin/pipenv install
/usr/local/bin/pipenv run gunicorn --bind 0.0.0.0:5000 --access-logfile - main:app &
