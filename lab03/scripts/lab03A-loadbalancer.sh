#!/bin/sh
cp /vagrant/files/nginx-loadbalancer.conf /etc/nginx/nginx.conf
sudo mkdir -p /etc/ssl/private /etc/ssl/certs
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/localhost.key -out /etc/ssl/certs/localhost.crt -subj "/C=BE/ST=Antwerp/L=Antwerp/O=Inuits/OU=Inuits/CN=lab02a"
