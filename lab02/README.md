# hostconfig.yml
Add portforwarding, ips and hostnames here

# scripts/{hostname}.sh
Add specific scripts for separate hosts
  - For the webservers: Copying the index file
  - For the loadbalancer: Generating the certificates

# scripts/common.sh
Add scripts that are shared between hosts
  - In this case, starting up the nginx service

# nginx-loadbalancer.conf
nginx configuration for the loadbalancer

# lab02{B,C}-index.html
index files for both webservers

# How to spin up?
Follow instructions in README.md of lab1 to create base box.
Then go to lab02 folder and run:
```
$ vagrant up
```
