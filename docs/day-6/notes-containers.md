When to use a VM and when to use a container?
- Use VM when you need kernel access
- If the application can be isolated, use a container
- There are hybrid solutions even
- No one true way to decide

Exercise:
Which version of python is on Debian Buster?
- 3.7.3

Which version of python is on OpenSUSE Leap?
- 3.6.10
