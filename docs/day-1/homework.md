TODO: References, titels, code blocks en line-wrap

Qemu: Open source Emulator and virtualizer used to run virtual machines using KVM and (less often Xen) with performance comparable to running natively
* I personally already use qemu from the commandline to spin up virtual machines for linux distros that seem interesting to me. My flow for spinning up a local vm goes as follows:
1. Download the `iso` file of the distribution of interest
2. Create a `qcow2` or `raw` image file using `qemu-img`, I usually go for a size of `32G` which is pretty much always more than enough
  * Usually I go for a `qcow2` image because it initially takes up less space on my HDD, but in some cases such as when installing `Pop!_OS` the installer might fail because there is not enough room available, in such cases I go for `raw`.
3. Start the virtual machine with the `iso` file attached with the `-cdrom` option, e.g. `qemu-system-x86_64 -enable-kvm -cdrom /data/isos/cool-distro.iso -usb -device usb-tablet -drive file=CoolDistro.qcow2,format=qcow2 --cpu host -smp 4 -m 4G -vga virtio -display gtk,gl=on`
* `-enable-kvm` to make use of `KVM` (Kernel based Virtual Machine) for near-native performance
* `-cdrom /data/isos/....` to use the `iso`
* `-usb -device usb-tablet` makes the mouse behave more like a usb-tablet, which in turn makes using a mouse in a vm behave a bit more sane (no sudden offsets)
* `4G` is more than enough memory for installing and running a distro
* 4 cores, same as above
* `-vga virtio` gives me the best performance and allows me to use the full resolution of any of my monitors.
  * `-vga qxl` can also be used to make use of the full resolution of all my monitors but gives me horrible performance.
* `-display gtk,gl=on`
  * `gtk` to have a useful menu bar at the top, if that is not wanted I use `sdl`
  * `gl=on` admittedly I don't entirely know what this part does but I assume that it is used to make use of 3D acceleration using OpenGL. I have found that this sometimes results in weird behavior in `KDE` and even more in `GNOME`. I encountered strange behavior regarding the mouse cursor when installing CentOS 8 with a GUI. Setting `gl=off` fixed that issue (this might be useful to mention if some of the other trainees have trouble with `GNOME` in a VM).
4. Once the VM is installed, I run it with the same command but without the `-cdrom` option

virt-manager is a desktop application for managing virtual machines with libvrt.
* It can handle KVM, Xen and LXC.

According to the libvirt website, the libvirt project is, among other things, an open source toolkit to manage virtualisation platforms.
* Libvirtd on the other hand is the daemon part of this virtualisation toolkit

If I can find the time tomorrow morning I will write/send you more research about virsh and vagrant.
