# lab08
Create a container which:
- [x] Is persistent bewtween starts and stops
- [x] Runs in background
- [x] Shares a directory with the host
- [x] Serves a custom static index.html page
- [x] The custom html page is located in the shared directory

## Dockerfile
- Base image: **CentOS8**
- Install `nginx`
- Copy `entrypoint.sh` script
- Expose port `80`

## entrypoint.sh
- Remove default `index.html` page
- Symlink `index.html` from shared folder (`/files`) to `/usr/share/nginx/html/index.html` so the file can be edited outside of the container
- Start `nginx`
- Keep container alive by running `/bin/bash`

## How to Run?
1. First build the docker image by running the provided script
```bash
$ ./build-and-tag-image.sh
```

2. Then run the container:
```bash
$ ./start-container.sh
```
