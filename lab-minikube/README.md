# Lab minikube
Purpose of this lab is to get more familiar with minikube and kubernetes.
I made a webserver with an index page that contains it's hostname, the hostname is dynamically inserted though [SSI](https://nginx.org/en/docs/http/ngx_http_ssi_module.htm://nginx.org/en/docs/http/ngx_http_ssi_module.html)

## How to run this
1. Set up [minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/)
2. Set up [kvm2](https://minikube.sigs.k8s.io/docs/drivers/kvm2/)
3. Start minikube
```bash
$ minikube start driver=kvm2
```
4. If you build the docker image regularly, minikube won't be able to find it, it uses it's own local list of images. So in order to build it you need to change some environment variables by running:
```bash
$ eval $(minikube docker-env)
```
5. Build the image with:
```bash
$ docker build -t minikube-webserver minikube-webserver/
```
6. Deploy the deployment with:
```bash
$ kubectl apply -f deployment.yaml
```
7. Expose the deployment with:
```bash
$ kubectl apply -f service.yaml
```
8. Test the application with:
```bash
$ minikube service basic-webservice
```
  - You may need to wait a few minutes or refresh *very* rapidly to see the hostname change. Seeing the hostname change indicates that the loadbalancer is doing it's job.


## minikube-webserver
This is a Docker image that is based on the `nginx:latest` image. It contains a different `index.html` page and `conf.d/default.conf`, to make use of SSI.

## deployment.yaml
This is a very basic kubernetes Deployment that contains some boilerplate to get 3 replicas of `minikube-webserver` up and make them available/selectable (?) under the name basic-webserver. The only thing of note here is:
- `imagePullPolicy: Never`: This is to ensure minikube uses the locally available images.

## service.yaml
A very basic kubernetes Service to create a LoadBalancer for the Pods created by applying `deployment.yaml`
