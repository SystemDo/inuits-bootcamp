# Provisioning of the VM
The provisioning was done using the "shell" provisioner inside the `Vagrantfile`

# Commands issued inside the vm
See inline script in `Vagrantfile`

# Installing vbox guest additions
```
$ vagrant plugin install vagrant-vbguest
```

# Turning VM into base box
```
$ vagrant package --output lab03-base
$ vagrant box add lab03-base lab03-base
```
